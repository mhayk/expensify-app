import * as firebase from 'firebase'

// const config = {
//     apiKey: "AIzaSyAa8xTdlEhEpG9KxX9_tf8Xq8EzKcDcYng",
//     authDomain: "expensify-3dee6.firebaseapp.com",
//     databaseURL: "https://expensify-3dee6.firebaseio.com",
//     projectId: "expensify-3dee6",
//     storageBucket: "expensify-3dee6.appspot.com",
//     messagingSenderId: "570701207120"
// };

const config = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
  };

firebase.initializeApp(config);

const database = firebase.database()

export { firebase, database as default }

// database.ref('expenses')
//   .once('value')
//   .then((snapshot) => {
//     console.log(snapshot.val())
//   })

// database.ref('expenses').push({
//   description: 'My trip to Brazil',
//   note: 'I will travel next year...',
//   amount: 32.00,
//   createdAt: ''
// })

// const notes = [
//   {
//     body: 'This is my note',
//     id: "12",
//     title: 'First note!'
//     },
//     {
//       body: 'This is my note',
//       id: '761ase',
//       title: 'Another note'
//     }
// ]

// database.ref('notes').set(notes)

// database.ref().on('value', (snapshot) => {
//   const {name, location} = snapshot.val()
//   console.log(`${name} is a ${location.city} at ${location.country}`)
// })

// database.ref().set({
//     name: 'Mhayk Whandson',
//     age: 32,
//     isSingle: false,
//     location: {
//         city: 'London',
//         country: 'United Kingdom'
//     }
// }).then(() => {
//   console.log('Data is saved')
// })
// .catch( (e) => {
//   console.log('.....Error: ', e)
// })

// database.ref().set('This is my data.')
// database.ref('age').set(33)
// database.ref('location/city').set('Boa Vista')

// database.ref('attributes').set({
//     height: 171,
//     weight: 90
// })
// .then(() => console.log('Second set call worked.'))
// .catch((e) => console.log('error:', e))

// database.ref().update({
//   name: 'Alice Lima',
//   age: 4,
//   company: 'Kau Media Group',
//   isSingle: null
// })

// var adaRef = database.ref()
// adaRef.remove()
//   .then(() => console.log('Remove succeeded'))
//   .catch(() => console.log('Remove failed: ' + error.message))