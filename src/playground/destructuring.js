// const person = {
//     name: 'Mhayk',
//     age: 31,
//     location: {
//         city: 'London',
//         temp: 13
//     }
// }

// const { name : firstName = 'Anonymous', age } = person
// console.log(`${firstName} is ${age}.`)
// const name = person.name;
// const age = person.age;

// const {city, temp: temperature } = person.location;
// if (city && temperature)
//     console.log(`It's ${temperature} in ${city}.`)

// const book = {
//     title: 'Book ABC',
//     author: 'Goku',
//     publisher: {
//         // name: 'Penguin'
//     }
// }

// const { name: publisherName = 'Self-Published' } = book.publisher

// console.log(publisherName)

//
// Array destructuring
//

// const address = ['58 Crimsworth Road', 'South Lambeth', 'London', 'SW84RL']

// const [street, city, state, zip] = address

// console.log(`You are in ${city} ${state}`)

const item = ['Coffee (acied)', '$2.00', '$3.50', '$2.75']
const [itemName, ,mediumPrice] = item
console.log(`A medium ${itemName} costs ${mediumPrice}`)